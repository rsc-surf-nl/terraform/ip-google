resource "google_compute_address" "static" {
  provider = google-beta
  name     = "src-${var.workspace_id}"
  project  = var.google_project
  region   = var.region
  labels   = {
    subscription       = lower(var.subscription),
    application_type   = lower(var.application_type),
    cloud_type         = lower(var.cloud_type),
    resource_type      = lower(var.resource_type),
    subscription_group = lower(var.subscription_group),
    workspace_id       = lower(var.workspace_id),
  }
}
