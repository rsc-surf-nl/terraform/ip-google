# Variables used for tagging in metadata
variable description {
  default = ""
}

# Variables used for resource configuraiton
variable region {}

# Google-specific variables
variable google_project {}
variable google_credentials {}
