output "id" {
  value = google_compute_address.static.id
}

output "ip_id" {
  value = google_compute_address.static.id
}

output "address" {
  value = google_compute_address.static.address
}

output "description" {
  value = var.description
}
